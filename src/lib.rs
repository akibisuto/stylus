#[cfg(feature = "warp")]
use warp::{
    http::header::{HeaderValue, CACHE_CONTROL, CONTENT_TYPE},
    hyper::Body,
    reply::Response,
    Filter, Rejection, Reply,
};

#[cfg(feature = "warp")]
pub fn route() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    assets().or(fonts()).or(min_assets())
}

pub static CSS: &str = include_str!("../css/stylus.css");
pub static MAP: &str = include_str!("../css/stylus.css.map");

#[cfg(feature = "warp")]
pub fn assets() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path("assets").and(
        warp::path("stylus.css")
            .map(|| MimeReply::css(CSS))
            .or(warp::path("stylus.css.map").map(|| MimeReply::map(MAP))),
    )
}

pub static MIN_CSS: &str = include_str!("../css/stylus.min.css");
pub static MIN_MAP: &str = include_str!("../css/stylus.min.css.map");

#[cfg(feature = "warp")]
pub fn min_assets() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path("assets").and(
        warp::path("stylus.min.css")
            .map(|| MimeReply::css(MIN_CSS))
            .or(warp::path("stylus.min.css.map").map(|| MimeReply::map(MIN_MAP))),
    )
}

pub static FONT_REGULAR_TTF: &[u8] = include_bytes!("../fonts/noto-sans-regular.ttf");
pub static FONT_REGULAR_WOFF: &[u8] = include_bytes!("../fonts/noto-sans-regular.woff");
pub static FONT_REGULAR_WOFF2: &[u8] = include_bytes!("../fonts/noto-sans-regular.woff2");

pub static FONT_ITALIC_TTF: &[u8] = include_bytes!("../fonts/noto-sans-italic.ttf");
pub static FONT_ITALIC_WOFF: &[u8] = include_bytes!("../fonts/noto-sans-italic.woff");
pub static FONT_ITALIC_WOFF2: &[u8] = include_bytes!("../fonts/noto-sans-italic.woff2");

pub static FONT_BOLD_TTF: &[u8] = include_bytes!("../fonts/noto-sans-bold.ttf");
pub static FONT_BOLD_WOFF: &[u8] = include_bytes!("../fonts/noto-sans-bold.woff");
pub static FONT_BOLD_WOFF2: &[u8] = include_bytes!("../fonts/noto-sans-bold.woff2");

pub static FONT_BOLD_ITALIC_TTF: &[u8] = include_bytes!("../fonts/noto-sans-bold-italic.ttf");
pub static FONT_BOLD_ITALIC_WOFF: &[u8] = include_bytes!("../fonts/noto-sans-bold-italic.woff");
pub static FONT_BOLD_ITALIC_WOFF2: &[u8] = include_bytes!("../fonts/noto-sans-bold-italic.woff2");

#[cfg(feature = "warp")]
pub fn fonts() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path("fonts").and(ttf().or(woff()).or(woff2()))
}

#[cfg(feature = "warp")]
fn ttf() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::get().and(
        warp::path("noto-sans-regular.ttf")
            .map(|| MimeReply::font(FONT_REGULAR_TTF))
            .or(warp::path("noto-sans-italic.ttf").map(|| MimeReply::font(FONT_ITALIC_TTF)))
            .or(warp::path("noto-sans-bold.ttf").map(|| MimeReply::font(FONT_BOLD_TTF)))
            .or(warp::path("noto-sans-bold-italic.ttf")
                .map(|| MimeReply::font(FONT_BOLD_ITALIC_TTF))),
    )
}

#[cfg(feature = "warp")]
fn woff() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::get().and(
        warp::path("noto-sans-regular.woff")
            .map(|| MimeReply::font(FONT_REGULAR_WOFF))
            .or(warp::path("noto-sans-italic.woff").map(|| MimeReply::font(FONT_ITALIC_WOFF)))
            .or(warp::path("noto-sans-bold.woff").map(|| MimeReply::font(FONT_BOLD_WOFF)))
            .or(warp::path("noto-sans-bold-italic.woff")
                .map(|| MimeReply::font(FONT_BOLD_ITALIC_WOFF))),
    )
}

#[cfg(feature = "warp")]
fn woff2() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::get().and(
        warp::path("noto-sans-regular.woff2")
            .map(|| MimeReply::font(FONT_REGULAR_WOFF2))
            .or(warp::path("noto-sans-italic.woff2").map(|| MimeReply::font(FONT_ITALIC_WOFF2)))
            .or(warp::path("noto-sans-bold.woff2").map(|| MimeReply::font(FONT_BOLD_WOFF2)))
            .or(warp::path("noto-sans-bold-italic.woff2")
                .map(|| MimeReply::font(FONT_BOLD_ITALIC_WOFF2))),
    )
}

#[cfg(feature = "warp")]
struct MimeReply<T> {
    body: T,
    mime: &'static str,
}

#[cfg(feature = "warp")]
impl<T> MimeReply<T> {
    fn css(body: T) -> Self {
        Self {
            body,
            mime: "text/css; charset=utf-8",
        }
    }

    fn font(body: T) -> Self {
        Self {
            body,
            mime: "application/octet-stream",
        }
    }

    fn map(body: T) -> Self {
        Self {
            body,
            mime: "application/json; charset=utf-8",
        }
    }
}

#[cfg(feature = "warp")]
impl<T> Reply for MimeReply<T>
where
    Body: From<T>,
    T: Send,
{
    #[inline]
    fn into_response(self) -> Response {
        let mut res = Response::new(Body::from(self.body));

        res.headers_mut().insert(
            CACHE_CONTROL,
            HeaderValue::from_static("public, max-age=37260"),
        );

        res.headers_mut()
            .insert(CONTENT_TYPE, HeaderValue::from_static(self.mime));

        res
    }
}
